import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import PropTypes from 'prop-types';
import PointSizes from './components/PointSizes';
import RandomAnimatedLine from './components/RandomAnimatedLine';
import RandomAnimatedBars from './components/RandomAnimatedBars';
import PolarArea from './components/PolarArea';
import DynamiclyRefreshedDoughnut from './components/DynamiclyRefreshedDoughnut';
import LegendHandlers from './components/LegendHandlers';

const ChartsJS = () => (
  <Container>
    <Row>
      <Col md={12}>
        <h3 className="page-title">Risk Cockpit</h3>
        <h3 className="page-subhead subhead">First glance look at assets and risks that require your attention.
        </h3>
      </Col>
    </Row>
    <Row>
      <PointSizes />
      <RandomAnimatedLine />
      <RandomAnimatedBars />
      <PolarArea />
      <DynamiclyRefreshedDoughnut />
      <LegendHandlers />
    </Row>
  </Container>
);


export default ChartsJS;
