import React, { PureComponent } from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import { Doughnut } from 'react-chartjs-2';
import PropTypes from 'prop-types';

function getRandomInt(min, max) {
  return Math.floor(Math.random() * ((max - min) + 1)) + min;
}

const getState = () => ({
  labels: [
    'Confidentiality',
    'Integrity',
    'Availability',
  ],
  datasets: [{
    data: [getRandomInt(50, 200), getRandomInt(100, 150), getRandomInt(150, 250)],
    backgroundColor: [
      '#FF6384',
      '#36A2EB',
      '#FFCE56',
    ],
    hoverBackgroundColor: [
      '#FF6384',
      '#36A2EB',
      '#FFCE56',
    ],
    borderColor: 'rgba(255,255,255,0.54)',
  }],
});

class DynamiclyRefreshedDoughnut extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      data: getState(),
      intervalId: null,
    };
  }

  componentWillMount() {
    const intervalId = setInterval(() => {
      this.setState({ data: getState() });
    }, 4000);

    this.setState({ intervalId });
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  render() {

    return (
      <Col md={12} lg={12} xl={6}>
        <Card>
          <CardBody>
            <div className="card__title">
              <h5 className="bold-text">Single Impact ratings</h5>
            </div>
            <Doughnut data={this.state.data} />
          </CardBody>
        </Card>
      </Col>
    );
  }
}

export default DynamiclyRefreshedDoughnut;
