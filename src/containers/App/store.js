import { combineReducers, createStore, applyMiddleware } from 'redux';
import { sidebarReducer, themeReducer } from '../../redux/reducers/index';
import {assetsReducer} from '../../riskckpit/redux-reducers/assetsReducer';
import thunkMiddleware from 'redux-thunk';

const reducer = combineReducers({
  theme: themeReducer,
  sidebar: sidebarReducer,
  assets: assetsReducer,
});

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

export default store;
