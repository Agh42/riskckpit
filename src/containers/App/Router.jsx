import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Layout from '../Layout/index';
import MainWrapper from './MainWrapper';

import LogIn from '../LogIn/index';
import AssetsPage from '../../riskckpit/pages/AssetsPage';
import AssetFormPage from '../../riskckpit/pages/AssetFormPage';
import AssetTimelinePage from '../../riskckpit/pages/AssetTimelinePage';
import ScenariosPage from '../ScenariosPage/index';

import ChartsJS from '../Charts/ChartJs/index';

const Pages = () => (
  <Switch>
    <Route path="/pages/charts" component={ChartsJS} />
    <Route path="/pages/assets" component={AssetsPage} />
    <Route path="/pages/scenarios" component={ScenariosPage} />
    <Route path="/pages/asset" component={AssetFormPage} />
    <Route path="/pages/asset-outatime" component={AssetTimelinePage} />
  </Switch>
);

const wrappedRoutes = () => (
  <div>
    <Layout />
    <div className="container__wrap">
      <Route exact path="/" component={ChartsJS} />
      <Route path="/pages" component={Pages} />
    </div>
  </div>
);

const Router = () => (
  <MainWrapper>
    <main>
      <Switch>
        <Route exact path="/log_in" component={LogIn} />
        <Route path="/" component={wrappedRoutes} />
      </Switch>
    </main>
  </MainWrapper>
);

export default Router;
