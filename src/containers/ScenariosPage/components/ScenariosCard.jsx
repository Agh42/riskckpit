import React from 'react';
import { Card, CardBody, Col } from 'reactstrap';

const ScenariosCard = () => (
  <Col md={12}>
    <Card>
      <CardBody>
        <div className="card__title">
          <h5 className="bold-text">Risk Scenarios</h5>
          <h5 className="subhead">Manage your risk scenarios here.</h5>
        </div>
        <p>Your content here</p>
      </CardBody>
    </Card>
  </Col>
);

export default ScenariosCard;
