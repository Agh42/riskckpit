import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import ScenariosCard from './components/ScenariosCard';

const ScenariosPage = () => (
  <Container className="dashboard">
    <Row>
      <Col md={12}>
        <h3 className="page-title">Risk Scenarios</h3>
      </Col>
    </Row>
    <Row>
      <ScenariosCard />
    </Row>
  </Container>
);

export default ScenariosPage;
