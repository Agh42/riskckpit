import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SidebarLink from './SidebarLink';
import SidebarCategory from './SidebarCategory';

class SidebarContent extends Component {
  static propTypes = {
    changeToDark: PropTypes.func.isRequired,
    changeToLight: PropTypes.func.isRequired,
    onClick: PropTypes.func.isRequired,
  };

  hideSidebar = () => {
    this.props.onClick();
  };

  // ak note: NavLink uses lnr icons, see linearicons.com/free 

  render() {
    return (
      <div className="sidebar__content">
       <ul className="sidebar__block">
          <SidebarLink title="Risk Cockpit" icon="pie-chart" route="/pages/charts" onClick={this.hideSidebar} />
           <SidebarCategory title="Assets" icon="laptop-phone">
                <SidebarLink title="Assets Worksheet" route="/pages/assets" onClick={this.hideSidebar} />
                <SidebarLink title="Drafts"  route="/pages/asset-drafts" onClick={this.hideSidebar} />
                <SidebarLink title="Time Travel"  route="/pages/asset-outatime" onClick={this.hideSidebar} />
           </SidebarCategory>
          <SidebarLink title="Risk Scenarios" icon="warning" route="/pages/scenarios" onClick={this.hideSidebar} />
        </ul>
        <ul className="sidebar__block">
          <SidebarCategory title="Layout" icon="layers">
            <button className="sidebar__link" onClick={this.props.changeToLight}>
              <p className="sidebar__link-title">Light Theme</p>
            </button>
            <button className="sidebar__link" onClick={this.props.changeToDark}>
              <p className="sidebar__link-title">Dark Theme</p>
            </button>
          </SidebarCategory>
        </ul>
       
      </div>
    );
  }
}

export default SidebarContent;
