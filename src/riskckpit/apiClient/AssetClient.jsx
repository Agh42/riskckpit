const AssetClient = {loadAssetTimeline, saveAsset};
export default AssetClient;



/*
 Called by action fetchAssetTimeline with callback.
 */
function loadAssetTimeline() {
  return {
    then: function (callback) {
      setTimeout(() => {
        callback(JSON.parse(localStorage.assetTimeline || '[]'))
      }, 1000);
    }
  }
}

function  saveAsset(asset) {

  return new Promise(function (resolve, reject) {
    setTimeout(() => {
      localStorage.asset = JSON.stringify(asset);
      resolve( 'success' );
    }, 1000);
  })
}


