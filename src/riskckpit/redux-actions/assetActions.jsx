import AssetClient from '../apiClient/AssetClient';

export const ASSET_CREATE = 'ASSET_CREATE';
    export function assetCreate() {
      return {
        type: ASSET_CREATE,
      };
    }
export const ASSET_EDIT = 'ASSET_EDIT';
export const ASSET_DEL = 'ASSET_DEL';
export function assetDel() {
    return {
        type: ASSET_DEL,
    };
}
export const ASSET_SAVE_DRAFT_REQUEST = 'ASSET_SAVE_DRAFT_REQUEST';
export const ASSET_SAVE_DRAFT_SUCESS = 'ASSET_SAVE_DRAFT_SUCCESS';
export const ASSET_SAVE_DRAFT_FAILURE = 'ASSET_SAVE_DRAFT_FAILURE';

export const ASSET_SAVE_REQUEST = 'ASSET_SAVE_REQUEST';
function assetSaveRequest() {
    return {type: ASSET_SAVE_REQUEST};
}

export const ASSET_SAVE_SUCCESS = 'ASSET_SAVE_SUCCESS';
function assetSaveSuccess(asset) {
    return {type: ASSET_SAVE_SUCCESS, asset};
}

export const ASSET_SAVE_FAILURE = 'ASSET_SAVE_FAILURE';
function assetSaveFailure(error) {
    return {type: ASSET_SAVE_FAILURE, error};
}

export const ASSET_SHOW_HISTORY = 'ASSET_SHOW_HISTORY';
export const ASSET_PAGINATION_CHANGE = 'ASSET_PAGINATION_CHANGE';
export function assetPageChanged(pageOfItems) {
    return {
        type: ASSET_PAGINATION_CHANGE,
        pageOfItems: pageOfItems,
    };
}
export const ASSET_SELECTION_CHANGE = 'ASSET_SELECTION_CHANGE';
export function assetSelectionChanged(selectedItems) {
    return {
        type: ASSET_SELECTION_CHANGE,
        selectedItems: selectedItems,
    }
}

export const ASSET_LOAD_TIMELINE_REQUEST = 'ASSET_LOAD_TIMELINE_REQUEST';
function assetLoadTimeLineRequest() {
    return {type: ASSET_LOAD_TIMELINE_REQUEST};
}

export const ASSET_LOAD_TIMELINE_SUCCESS = 'ASSET_LOAD_TIMELINE_SUCCESS';
function assetLoadTimeLineSuccess(assetTimeline) {
    return {type: ASSET_LOAD_TIMELINE_SUCCESS};
}

export function saveAsset (asset) {
  return function (dispatch) {
    dispatch(assetSaveRequest())
    AssetClient.saveAsset(asset)
      .then((resp) => { dispatch(assetSaveSuccess(asset)) })
      .catch((err) => { dispatch(assetSaveFailure(err)) })
  }
}

export function fetchAssetTimeline () {
  return function (dispatch) {
    dispatch(assetLoadTimeLineRequest())
    AssetClient.loadAssetTimeline.then((assetTimeline) => {
      dispatch(assetLoadTimeLineSuccess(assetTimeline))
    })
  }
}





