import {
    ASSET_SELECTION_CHANGE,
} from '../redux-actions/assetActions';


export function selectedItemsReducer(state = [], action) {
    switch (action.type) {
        case ASSET_SELECTION_CHANGE: {
            return action.selectedItems;
        }
        default:
            return state;
    }
}