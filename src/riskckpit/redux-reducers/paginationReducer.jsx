import {
    ASSET_PAGINATION_CHANGE,
} from '../redux-actions/assetActions';


export function paginationReducer(state = [], action) {
    switch (action.type) {
        case ASSET_PAGINATION_CHANGE: {
            // return  action.pageOfItems;
            return state; // component receives rows as props, no further change needed
        }
        default:
            return state;
    }
}