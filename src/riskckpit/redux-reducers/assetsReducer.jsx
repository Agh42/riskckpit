import {assetListReducer} from './assetListReducer';
import {paginationReducer} from './paginationReducer';
import {selectedItemsReducer} from './selectedItemsReducer';

import {
    ASSET_CREATE,
    ASSET_DEL,
    ASSET_PAGINATION_CHANGE,
    ASSET_EDIT,
    ASSET_PUBLISH,
    ASSET_SAVE_DRAFT,
    ASSET_SHOW_HISTORY,
    ASSET_SELECTION_CHANGE,
    ASSET_SAVE_REQUEST,
    ASSET_SAVE_SUCCESS,
    ASSET_SAVE_FAILURE,
    
} from '../redux-actions/assetActions';

export function assetsReducer(state = {
    assetList: assetListReducer(undefined, undefined, {}),
    pageOfItems: paginationReducer(undefined, {}),
    selectedItems: selectedItemsReducer(undefined, {}),
    asset: {
         title: '',
         lastModifiedBy: '',
         lastModified: new Date()
    },
    isLoading: false,
    saveStatus: 'READY',
}, action) {
    switch (action.type) {
        case ASSET_CREATE:
        case ASSET_DEL:
            const stateAfterAssetChange = {
                ...state,
                assetList: assetListReducer(state.assetList, state.selectedItems, action),
            }    
            return stateAfterAssetChange;
        case ASSET_PAGINATION_CHANGE:
            // do nothing - state managed inside component. full list provided as props.
            // const stateAfterPageChange = {
            //     ...state,
            //     pageOfItems: paginationReducer(state.pageOfItems, action),
            // }
            // return stateAfterPageChange;
            return state;
        case ASSET_SELECTION_CHANGE:
            const stateAfterSelectionChange = {
                ...state,
                selectedItems: selectedItemsReducer(state.selectedItems, action),
            }
            return stateAfterSelectionChange;
        case ASSET_SAVE_REQUEST:
            return {
                ...state,
                saveStatus: 'SAVING'
            }
        case ASSET_SAVE_FAILURE:
            return {
                ...state,
                saveStatus: 'ERROR'
            }
        case ASSET_SAVE_SUCCESS:
            return {
                ...state,
                asset: {
                    title: '',
                    lastModifiedBy: ''    ,
                    lastModified: null
                },
                saveStatus: 'SUCCESS'
            }
        default:
            return state;
    }
}