import {
    ASSET_CREATE,
    ASSET_DEL,
} from '../redux-actions/assetActions';

export function assetListReducer(state =[
      {
        id: "placeholder-asset",
        title: "...",
        confidentiality: 0 ,
        integrity: 0 ,
        availability: 0 ,
        lastmodified: new Date(),
        description: "...",
        risk: 0 ,
      },
    ], 
    selectedItems = [],
    action) {
    switch (action.type) {
        case (ASSET_CREATE):
             const newAsset = {
                id: new Date().getTime(), //TODO id will be created serverside later
                title: "New Asset",
                confidentiality: 0 ,
                integrity: 0 ,
                availability: 0 ,
                lastmodified: new Date(),
                description: "<Add Description>",
                risk: 0,
            };
            return state.concat(newAsset);
        case (ASSET_DEL):
            return state.filter(a => ! selectedItems.includes(a._id) );
        default:
            return state;
    }
}