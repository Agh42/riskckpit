/* eslint-disable react/no-unused-state,react/no-unescaped-entities */
import React from 'react';
import { Card, CardBody, Col, Button, ButtonToolbar, ButtonGroup } from 'reactstrap';
import EditableTable from '../../shared/components/table/EditableTable';
import Pagination from '../../shared/components/pagination/Pagination';
import PlusBoxIcon from 'mdi-react/PlusBoxIcon';
import FolderPlusIcon from 'mdi-react/FolderPlusIcon';
import LibraryPlusIcon from 'mdi-react/LibraryPlusIcon';
import FolderMoveIcon from 'mdi-react/FolderMoveIcon';
import DatabaseImportIcon from 'mdi-react/DatabaseImportIcon';
import DatabaseExportIcon from 'mdi-react/DatabaseExportIcon';
import DeleteIcon from 'mdi-react/DeleteIcon';
import DeleteRestoreIcon from 'mdi-react/DeleteRestoreIcon';
import UpdateIcon from 'mdi-react/UpdateIcon';


export const SortableTable = (props) => (
    <Col md={12} lg={12}>
        <Card>
          <CardBody>
            <p>Assets -> All Assets</p>
           <ButtonToolbar>
                <Button className="icon"  color="primary" onClick={props.onCreateClicked}><p><PlusBoxIcon/>Create...</p></Button>
                <Button className="icon"  color="danger" disabled={props.selectedItems.length<1} 
                    onClick={props.onDeleteClicked}><p><DeleteIcon/>Remove</p></Button>
                <Button className="icon"  color="primary" ><p><DeleteRestoreIcon/>Restore...</p></Button>
                <Button className="icon"  color="primary" ><p><UpdateIcon/>Time Travel...</p></Button>
                <Button className="icon"  color="primary"><p><LibraryPlusIcon/>Copy</p></Button>
                <Button className="icon"  color="primary"><p><FolderMoveIcon/>Move...</p></Button>
                <Button className="icon"  color="success"><p><FolderPlusIcon/>New Folder...</p></Button>
                <Button className="icon"  color="primary"><p><DatabaseImportIcon/>Import...</p></Button>
                <Button className="icon"  color="primary"><p><DatabaseExportIcon/>Export...</p></Button>
           </ButtonToolbar>
            {/* { // debug output:
              props.rows.map((row, index) => (
                <p>{index} {row._id} {row.title}</p>
              ))
            } */}
            <EditableTable 
                heads={props.heads} 
                rows={props.rows} 
                onSelectionChanged={props.onSelectionChanged} />
            <Pagination items={props.rows} onChangePage={props.onChangePage} />
          </CardBody>
        </Card>
      </Col>
);
