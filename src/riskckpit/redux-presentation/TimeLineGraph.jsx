import React, { Component } from 'react';
import * as vis from 'vis';
import { Card, CardBody, Col } from 'reactstrap';

export default class TimeLinegraph extends Component {
   data;
   nodes;
   edges;

   componentDidMount() {
        this.initGraph(this.props);
    }

    initGraph = (props) => {
        let items = new vis.DataSet([
            {id: 1, content: 'Version 4', start: '2018-04-20'},
            {id: 2, content: 'Created', start: '2018-04-14'},
            {id: 3, content: 'Version 3', start: '2018-04-18'},
            {id: 4, content: 'Version 2', start: '2018-04-16'},
            {id: 5, content: 'Version 5', start: '2018-04-25'},
            {id: 6, content: 'Removed', start: '2018-04-27'}
        ]);
        let options = {};
        let container = document.getElementById('timelinegraph');
        new vis.Timeline(container, items, options);
    }
    
    // componentWillReceiveProps(nextProps) {
    //     console.log(nextProps);
    //     this.initGraph(nextProps);
    // }

    render() {
        return (
            <Col md={12} lg={12} xl={12} >
                <Card>
                    <CardBody>
                        <div id="timelinegraph" style={{"height":"100%"}}></div>
                    </CardBody>
                </Card>
            </Col>
        );
    }
}


