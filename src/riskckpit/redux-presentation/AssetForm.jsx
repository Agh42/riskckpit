import PropTypes from 'prop-types';
import { Card, CardBody, Col, Button, ButtonToolbar } from 'reactstrap';
import React from 'react';
//import { Field, reduxForm } from 'redux-form';
import FormField from './FormField';
import DatePicker from 'react-datepicker';
import CalendarBlankIcon from 'mdi-react/CalendarBlankIcon';

export default class AssetForm extends React.Component {
    static propTypes = {
        asset: PropTypes.array.isRequired,
        isLoading: PropTypes.bool.isRequired,
        saveStatus: PropTypes.string.isRequired,
        fields: PropTypes.object,
        onSubmit: PropTypes.func.isRequired
    };

    state = {
        fields: this.props.fields || {
            title: '',
            lastModifiedBy: '',
            lastModified: new Date(),
        },
        fieldErrors: {}
    };

    getDerivedStateFromProps = (update) => {
        //console.log('this.props.fields', this.props.fields, update);
        return { fields: update.fields };
    }

    onFormSubmit = evt => {
        const asset = this.state.fields;
        evt.preventDefault();
        if (this.validate()) return;
        this.props.onSubmit(asset);
    };

    onInputChange = ({ name, value, error }) => {
        const fields = this.state.fields;
        const fieldErrors = this.state.fieldErrors;

        fields[name] = value;
        fieldErrors[name] = error;
        console.log("name_error:" + name+"_"+error);
        console.log("name_value:" + name+"_"+value);

        this.setState({ fields, fieldErrors });
    };

    onDateChange = (date) => {
        const name= 'lastModified';
        const error='';
        const value=date;
        console.log("date:" + value);
        this.onInputChange({name, value, error});
    }

    validate = () => {
        const asset = this.state.fields;
        const fieldErrors = this.state.fieldErrors;
        const errMessages = Object.keys(fieldErrors).filter(k => fieldErrors[k]);

        //console.log("errMsgs:" + errMessages);

        if (!asset.title) return true;
        if (errMessages.length) return true;

        return false;
    };

    render() {
        if (this.props.isLoading) {
            return <div>Loading...</div>;
        }

        const dirty = Object.keys(this.state.fields).length;
        let status = this.props.saveStatus;
        if (status === 'SUCCESS' && dirty) status = 'READY';

        return (
            <Col md={12} lg={12}>
                <Card>
                    <CardBody>
                        <form className="form" onSubmit={this.onFormSubmit}>
                            <div className="form__half">
                                <div className="form__form-group">
                                    <span className="form__form-group-label">Asset Title</span>
                                    <div className="form__form-group-field">
                                        <FormField
                                            placeholder="Title"
                                            name="title"
                                            value={this.state.fields.title}
                                            onChange={this.onInputChange}
                                            validate={val => (val ? false : 'Title is required')}
                                        />
                                    </div>
                                </div>

                                <div className="form__form-group">
                                    <span className="form__form-group-label">Last modified by</span>
                                    <div className="form__form-group-field">
                                        <FormField
                                            placeholder="Person"
                                            name="lastModifiedBy"
                                            value={this.state.fields.lastModifiedBy}
                                            onChange={this.onInputChange}
                                            validate={val => (val ? false : 'Author is required')}
                                        />
                                    </div>
                                </div>

                                <div className="form__form-group">
                                    <span className="form__form-group-label">Last modified</span>
                                    <div className="form__form-group-field">
                                        <div className="date-picker">
                                        <DatePicker
                                            className="form__form-group-datepicker"
                                            selected={this.state.fields.lastModified}
                                            onChange={this.onDateChange}
                                            dateFormat="yyyy-MM-dd"
                                        />
                                        </div>
                                        <div className="form__form-group-icon">
                                            <CalendarBlankIcon />
                                        </div>
                                    </div>
                                </div>

                                {
                                    {
                                        SAVING: (
                                            <ButtonToolbar className="form__button-toolbar">
                                                <Button color="primary" type="submit" disabled>Saving...</Button>
                                            </ButtonToolbar>),
                                        SUCCESS: (
                                            <ButtonToolbar className="form__button-toolbar">
                                                <Button color="primary" type="submit" disabled>Saved!</Button>
                                            </ButtonToolbar>),
                                        ERROR: (
                                            <ButtonToolbar className="form__button-toolbar">
                                                <Button color="primary" type="submit" disabled={this.validate()}>Save failed - retry?</Button>
                                            </ButtonToolbar>),
                                        READY: (
                                            <ButtonToolbar className="form__button-toolbar">
                                                <Button color="primary" type="submit" disabled={this.validate()}>Save</Button>
                                            </ButtonToolbar>)
                                    }[status]
                                }

                            </div>  {/* form-half */}
                        </form>
                    </CardBody>
                </Card>
            </Col>
        );
    }

}
