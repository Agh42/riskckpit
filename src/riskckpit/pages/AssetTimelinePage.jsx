import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import TimeLineGraph from '../redux-presentation/TimeLineGraph';
import {connect} from 'react-redux';
import AssetForm from '../redux-presentation/AssetForm';
import {saveAsset} from '../redux-actions/assetActions';
import {createStore, applyMiddleware} from 'redux';

// example for full asset object with draft and history:
// mongodb atomic update. fails if version 3 has already been updated:
/*
db.runCommand("findAndModify" : "assets",
    "query" : {"_id" : "xyz", "version": 3},
    "update" : { "$set" : {
      "$push" : {"history" : asset.current}
      {"$inc" : {"version" : 1} },
      "title" : "newtitle",
    }}
)
*/

const initialAsset = {
  _id: "uuid",

  current: {
    title: "New Asset",
    version: 3,
    lastModified: new Date(),
    lastModifiedBy: "Bob",
  },
  
  draft: {
    version: 1,
    title: "Future Asset",
    lastModified: new Date(),
    lastModifiedBy: "Alice",
  },
  
  history: [
    {
        version: 1,
        title: "Oldest Asset",
        lastModified: new Date(),
        lastModifiedBy: "Alice",
    },
    {
        version: 2,
        title: "Old Asset",
        lastModified: new Date(),
        lastModifiedBy: "Bob",
    }
  ]
};


const AssetFormContainer = connect(mapStateToProps, mapDispatchToProps)(AssetForm);

function mapStateToProps(state) {
  return {
    isLoading: state.assets.isLoading,
    fields: state.assets.asset,
    saveStatus: state.assets.saveStatus
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onSubmit: asset => {
      dispatch(saveAsset(asset));
    }
  };
}

const AssetTimelinePage = () => (
  <Container className="dashboard">
    <Row>
      <Col md={12}>
        <h3 className="page-title">Asset History</h3>
        <h3 className="page-subhead subhead">Go back in time</h3>
      </Col>
    </Row>
    <Row>
      <TimeLineGraph />
    </Row>
    <Row>
      <AssetFormContainer />
    </Row>
  </Container>
);


export default AssetTimelinePage;