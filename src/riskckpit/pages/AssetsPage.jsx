import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import {SortableTable} from '../redux-presentation/SortableTable';
import {connect} from 'react-redux';
import * as assetActions from '../redux-actions/assetActions';

const mapStateToTableProps = (state) => {
    const rows = state.assets.assetList.map(a => (
      {
        id: a.id ,
        title: a.title,
        confidentiality: a.confidentiality ,
        integrity: a.integrity ,
        availability: a.availability ,
        lastmodified: a.lastmodified.toLocaleTimeString(),
        description: a.description,
        risk: a.risk ,
      }
    ));

     const heads = [
       {
         key: 'icon',
         name: '',
         width: 50,
       },
      {
        key: 'id', 
        name: 'ID',
        width: 80,
      },
      {
        key: 'title',
        name: 'Title',
        sortable: true,
      },
      {
        key: 'confidentiality',
        name: 'Confidentiality',
        sortable: true,
      },
      {
        key: 'integrity',
        name: 'Integrity',
        sortable: true,
      },
      {
        key: 'availability',
        name: 'Availability',
        sortable: true,
      },
      {
        key: 'lastmodified',
        name: 'Last Modified',
        sortable: true,
      },
      {
        key: 'description',
        name: 'Description',
        sortable: true,
      },
      {
        key: 'risk',
        name: 'Risk',
        sortable: true,
      },
    ];

    const selectedItems = state.assets.selectedItems;

    return {
      heads: heads,
      rows: rows,
      selectedItems: selectedItems,
    };
};

const mapDispatchToTableProps = (dispatch) => (
    {
      onDeleteClicked: () => (
        dispatch(assetActions.assetDel())
      ),
      onCreateClicked: () => (
        dispatch(assetActions.assetCreate())
      ),
      onChangePage: (pageOfItems) => (
        dispatch(assetActions.assetPageChanged(pageOfItems))
      ),
      onSelectionChanged: (selectedItems) => (
        dispatch(assetActions.assetSelectionChanged(selectedItems))
      ),
    }
);

const SortableTableContainer = connect(
    mapStateToTableProps,
    mapDispatchToTableProps
)(SortableTable);


const AssetsPage = () => ( // pres comp
  <Container className="dashboard">
    <Row>
      <Col md={12}>
        <h3 className="page-title">Information Assets</h3>
      </Col>
    </Row>
    <Row>
     <SortableTableContainer />
    </Row>
  </Container>
);

export default AssetsPage;
