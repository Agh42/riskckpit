/* eslint-disable consistent-return */
import React, { Component } from 'react';
import ReactDataGrid from 'react-data-grid';
import update from 'immutability-helper';
import PropTypes from 'prop-types';
import PencilIcon from 'mdi-react/PencilIcon';

export default class EditableTable extends Component {

  // constructor(props, context) {
  //   super(props, context);
  //   const originalRows = this.props.rows;
  //   const rows = originalRows.slice(0, 10);
  //   this.state = { rows, originalRows, selectedIndexes: [] };
  // }

  constructor(props, context) {
    super(props, context);
    this.state = {
      rows: [],
      selectedIndexes: [],
    };
  }

  initTable(newProps) {
    this.setState({rows: newProps.rows});
  }

  componentDidMount() {
    this.initTable(this.props);
  }
  
  componentWillReceiveProps(nextProps) {
        this.initTable(nextProps);
    }

  onRowsSelected = (rows) => {
    this.setState({
      ...this.state,
      selectedIndexes: this.state.selectedIndexes.concat(
        rows.map(r => r.rowIdx)
      )
    });
    this.handleSelectionChanged();
  };
  
  handleSelectionChanged = () => {
    const selectedIDs = this.state.selectedIndexes.map(idx => (this.state.rows[idx].id) );
    //debug output
    console.log(selectedIDs);
    this.props.onSelectionChanged(selectedIDs); // FIXME xxx state change happening before dispatch
  }
  
  onRowsDeselected = (rows) => {
    let rowIndexes = rows.map(r => r.rowIdx);
    this.setState({
      ...this.state,
      selectedIndexes: this.state.selectedIndexes.filter(
        i => rowIndexes.indexOf(i) === -1
        )
      });
      this.handleSelectionChanged();
    };
    
    handleGridRowsUpdated = ({ fromRow, toRow, updated }) => {
      const rows = this.state.rows.slice();

    for (let i = fromRow; i <= toRow; i += 1) {
      const rowToUpdate = rows[i];
      rows[i] = update(rowToUpdate, { $merge: updated });
    }

    this.setState({...this.state,  rows });
  };

  handleGridSort = (sortColumn, sortDirection) => {
    const comparer = (a, b) => {
      if (sortDirection === 'ASC') {
        return (a[sortColumn] > b[sortColumn]) ? 1 : -1;
      } else if (sortDirection === 'DESC') {
        return (a[sortColumn] < b[sortColumn]) ? 1 : -1;
      }
    };

    const sortRows = this.state.rows.slice(0);
    const rows = sortDirection === 'NONE' ? this.state.rows.slice(0, 10) : sortRows.sort(comparer).slice(0, 10);

    this.setState({ ...this.state, rows });
  };

  iconCellActions = [
    {
      icon: <span><PencilIcon/></span>, 
      callback: () => {
        alert("Route to edit");
      }
    },
  ];

 getCellActions = (column, row) => {
  const cellActions = {
    icon: this.iconCellActions
  };
  return cellActions[column.key];
} 

  rowGetter = i => this.state.rows[i];

  render() {
    return (
      <div className="table">
        <ReactDataGrid
          onGridSort={this.handleGridSort}
          enableCellSelect
          columns={this.props.heads}
          rowGetter={this.rowGetter}
          rowsCount={this.state.rows.length}
          getCellActions={this.getCellActions}
          onGridRowsUpdated={this.handleGridRowsUpdated}
          rowHeight={44}
          minColumnWidth={100}
          rowSelection={{
            showCheckbox: true,
            enableShiftSelect: true,
            onRowsSelected: this.onRowsSelected,
            onRowsDeselected: this.onRowsDeselected,
            selectBy: {
              indexes: this.state.selectedIndexes
            }
          }}
        />
      </div>
    );
  }
}
